package bcoo;

import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class FenetreMere extends JFrame{
	public FenetreMere(String parTitre){
		
		super(parTitre);
		
		JPanel contentPane = new JPanel();
		setContentPane(contentPane);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setBackground(Color.GRAY);
		
		setSize(800,400);
		setLocation(new Point());
		setVisible(true);
	}
	
	public static void main(String[] args){
		FenetreMere fenetre = new FenetreMere("Ma fen�tre");
		JButton test = new JButton("test");
		fenetre.add(test);
	}
}
